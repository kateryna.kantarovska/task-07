package com.epam.rd.java.basic.task7.db;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

import com.epam.rd.java.basic.task7.db.entity.*;

import java.sql.*;
import java.util.Properties;


public class DBManager {

	private static DBManager instance;

	private static final String MESSAGE = "Exception occurred";

	public static synchronized DBManager getInstance() {
		if (instance == null)
			instance = new DBManager();
		return instance;
	}

	private DBManager() {
	}

	public Connection getConnection() throws DBException {
		Properties p = new Properties();
		Connection connection;
		try (FileReader reader = new FileReader("app.properties")){
			p.load(reader);
			connection = DriverManager.getConnection(p.getProperty("connection.url"));
		} catch (Exception ex) {
			throw new DBException(MESSAGE, ex);
		}
		return connection;
	}

	public List<User> findAllUsers() throws DBException {
		Connection connection = getConnection();
		String s = "SELECT * FROM users";
		List<User> users = new ArrayList<>();
		try (Statement statement = connection.createStatement()){
			ResultSet resultSet = statement.executeQuery(s);
			while (resultSet.next()) {
				User user = new User(resultSet.getInt("id"), resultSet.getString("login"));
				users.add(user);
			}
		} catch (SQLException e) {
			throw new DBException(MESSAGE, e);
		}
		return users;
	}

	public boolean insertUser(User user) throws DBException {
		Connection connection = getConnection();
		String s = String.format("INSERT INTO users VALUES (DEFAULT, '%s')", user.getLogin());
		int affectedRows;
		try (Statement statement = connection.createStatement()){
			affectedRows = statement.executeUpdate(s, Statement.RETURN_GENERATED_KEYS);
			user.setId(getGeneratedKey(statement.getGeneratedKeys()));
		} catch (SQLException e) {
			throw new DBException(MESSAGE, e);
		}
		return affectedRows > 0;
	}

	public boolean deleteUsers(User... users) throws DBException {
		int affectedRows = 0;
		try (Connection connection = getConnection(); Statement statement = connection.createStatement()) {

			for (User user : users) {
				String s = String.format("DELETE FROM users WHERE login = '%s'", user.getLogin());
				affectedRows += statement.executeUpdate(s);
			}

		} catch (SQLException e) {
			throw new DBException(MESSAGE, e);
		}
		return affectedRows > 0;
	}

	public User getUser(String login) throws DBException {
		String s = String.format("SELECT * FROM users WHERE login = '%s'", login);
		User user = null;
		try (Connection connection = getConnection(); Statement statement = connection.createStatement()){
			ResultSet resultSet = statement.executeQuery(s);
			while (resultSet.next()) {
				user = new User(resultSet.getInt("id"), resultSet.getString("login"));
			}
		} catch (SQLException e) {
			throw new DBException(MESSAGE, e);
		}
		return user;
	}

	public Team getTeam(String name) throws DBException {
		String s = String.format("SELECT * FROM teams WHERE name = '%s'", name);
		Team team = null;
		try (Connection connection = getConnection(); Statement statement = connection.createStatement()){
			ResultSet resultSet = statement.executeQuery(s);
			while (resultSet.next()) {
				team = new Team(resultSet.getInt("id"), resultSet.getString("name"));
			}
		} catch (SQLException e) {
			throw new DBException(MESSAGE, e);
		}
		return team;
	}

	public List<Team> findAllTeams() throws DBException {
		String s = "SELECT * FROM teams";
		List<Team> teams = new ArrayList<>();
		return getTeams(s, teams);
	}

	public boolean insertTeam(Team team) throws DBException {
		String s = String.format("INSERT INTO teams VALUES (DEFAULT, '%s')", team.getName());
		int affectedRows;
		try (Connection connection = getConnection(); Statement statement = connection.createStatement()){
			affectedRows = statement.executeUpdate(s, Statement.RETURN_GENERATED_KEYS);
			team.setId(getGeneratedKey(statement.getGeneratedKeys()));
		} catch (SQLException e) {
			throw new DBException(MESSAGE, e);
		}
		return affectedRows > 0;
	}

	public boolean setTeamsForUser(User user, Team... teams) throws DBException {
		int affectedRows = 0;
		Connection connection = getConnection();
		try (Statement statement = connection.createStatement()){
			connection.setAutoCommit(false);
			for (Team team : teams) {
				String s = String.format("INSERT INTO users_teams VALUES (%d, %d)", user.getId(), team.getId());
				affectedRows += statement.executeUpdate(s);
			}
			connection.commit();
		} catch (SQLException e) {
			try {
				connection.rollback();
				throw new SQLException();
			} catch (SQLException ex) {
				throw new DBException(MESSAGE, ex);
			}
		} finally {
			try {
				connection.close();
			} catch (SQLException e) {
				e.printStackTrace();
			}
		}
		return affectedRows > 0;
	}

	public List<Team> getUserTeams(User user) throws DBException {
		List<Team> userTeams = new ArrayList<>();
		String s = String.format("SELECT id, name " +
				"FROM users_teams INNER JOIN teams " +
				"ON team_id = id WHERE user_id =%d", user.getId());
		return getTeams(s, userTeams);
	}

	public boolean deleteTeam(Team team) throws DBException {
		String s = String.format("DELETE FROM teams WHERE name = '%s'", team.getName());
		int affectedRows;
		try (Connection connection = getConnection(); Statement statement = connection.createStatement()) {
			affectedRows = statement.executeUpdate(s);
		} catch (SQLException e) {
			throw new DBException(MESSAGE, e);
		}
		return affectedRows > 0;
	}

	public boolean updateTeam(Team team) throws DBException {
		String s = String.format("UPDATE teams SET name = '%s' WHERE id = %d", team.getName(), team.getId());
		int affectedRows;
		try (Connection connection = getConnection(); Statement statement = connection.createStatement()) {
			affectedRows = statement.executeUpdate(s);
		} catch (SQLException e) {
			throw new DBException(MESSAGE, e);
		}
		return affectedRows > 0;
	}

	public int getGeneratedKey (ResultSet generatedKeys) throws DBException {
		try {
			if (generatedKeys.next()) {
				return generatedKeys.getInt(1);
			}
		} catch (SQLException e) {
			throw new DBException(MESSAGE, e);
		}
		return 0;
	}

	private List<Team> getTeams(String s, List<Team> teams) throws DBException {
		try (Connection connection = getConnection(); Statement statement = connection.createStatement()){
			ResultSet resultSet = statement.executeQuery(s);
			while (resultSet.next()) {
				teams.add(new Team(resultSet.getInt("id"), resultSet.getString("name")));
			}
		} catch (SQLException e) {
			throw new DBException(MESSAGE, e);
		}
		return teams;
	}

}
